package np.com.ideabreed.servicesexample;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

import np.com.ideabreed.servicesexample.notification.NotificationViewActivity;

import static android.app.Notification.PRIORITY_DEFAULT;

public class MainActivity extends AppCompatActivity {
    AsyncDemo demo;
    LinearLayout linearLayout;
    Animation combine, fade, rotate, scale, translate;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        linearLayout = findViewById(R.id.anim_view);
        button = findViewById(R.id.notify);


        combine = AnimationUtils.loadAnimation(this, R.anim.combine);
        scale = AnimationUtils.loadAnimation(this, R.anim.scale);
        translate = AnimationUtils.loadAnimation(this, R.anim.translate);
        fade = AnimationUtils.loadAnimation(this, R.anim.fade);
        rotate = AnimationUtils.loadAnimation(this, R.anim.rotate);


        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){

                linearLayout.startAnimation(fade);

            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                sendNotification();
            }
        });

        fade.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation){

            }

            @Override
            public void onAnimationEnd(Animation animation){
                linearLayout.startAnimation(scale);
            }

            @Override
            public void onAnimationRepeat(Animation animation){

            }
        });
        scale.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation){

            }

            @Override
            public void onAnimationEnd(Animation animation){
                linearLayout.startAnimation(translate);
            }

            @Override
            public void onAnimationRepeat(Animation animation){

            }
        });


        demo = new AsyncDemo(this);
        demo.execute("Hello From Main Activity To AsyncTask");


    }

    private void sendNotification(){

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, "Test");
        builder.setSmallIcon(R.drawable.ic_notifications_active_black_24dp)
               .setContentTitle("Test Notification")
               .setContentText("Hello This is my first notification test")
               .setAutoCancel(false)
               .setPriority(PRIORITY_DEFAULT);


        Intent notifyIntent = new Intent(MainActivity.this, NotificationViewActivity.class);
        notifyIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        notifyIntent.putExtra("body", "Hello This is my first notification test");

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notifyIntent,
                                                                PendingIntent.FLAG_UPDATE_CURRENT);

        builder.setContentIntent(pendingIntent);

        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        assert manager != null;
        manager.notify(0, builder.build());


    }

    public void startPlay(View view){

        Intent intent = new Intent(MainActivity.this, MyService.class);
        startService(intent);

    }

    public void stopPlay(View view){
        Intent intent = new Intent(MainActivity.this, MyService.class);
        stopService(intent);

    }


}
