package np.com.ideabreed.servicesexample.notification;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import np.com.ideabreed.servicesexample.R;

public class NotificationViewActivity extends AppCompatActivity {

    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifaction_view);

        textView = findViewById(R.id.notification_text);
        Intent intent = getIntent();
        String text = intent.getStringExtra("body");
        textView.setText(text);


    }
}
